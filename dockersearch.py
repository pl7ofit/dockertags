#!/usr/bin/env python3

### LICENSE: GNU GENERAL PUBLIC LICENSE. Version 3 ###

from urllib import request as req
import argparse, sys, json, datetime, time

PAGE=0
PAGE_SIZE=100
TIME_NOW=datetime.datetime.timestamp(datetime.datetime.now())

# https://hub.docker.com/search?q=alpine&type=image&operating_system=linux&architecture=386%2Camd64&_data=root

parser = argparse.ArgumentParser(
                    prog='ProgramName',
                    description='What the program does',
                    epilog='Text at the bottom of help')
parser.add_argument('-s', '--search', required=True)
parser.add_argument('-a', '--arch', default="amd64")
parser.add_argument('-o', '--os', default="linux")
parser.add_argument('-S', '--sort', default="pulls")
parser.add_argument('-t', '--type', default="image")
parser.add_argument('-r', '--reverse', action='store_true')
parser.add_argument('-m', '--min-pulls', default=1000, type=int)
parser.add_argument('-u', '--updated-over-last-days', default=3650, type=int)
parser.add_argument('-R', '--max-results', default=2500, type=int)
parser.add_argument('-T', '--min-stars', default=0, type=int)
parser.add_argument('-V', '--verbose', action='store_true')
args = parser.parse_args()


base_url = f"https://hub.docker.com/api/search/v3/catalog/search?"
url_args=f"query={args.search}&type={args.type}&operating_system={args.os}&architecture={args.arch}"
url_with_args = f"{base_url}{url_args}"

def to_unixtime(rfc3339):
    return int(datetime.datetime.timestamp(datetime.datetime.strptime(rfc3339, "%Y-%m-%dT%H:%M:%S.%fZ")))

def format_pull_count(pulls):
  mul = 1
  if "B+" in pulls:
    mul = 1000000000
  elif "M+" in pulls:
    mul = 1000000
  elif "K+" in pulls:
    mul = 1000
  return int(float(pulls.replace("K", "").replace("M", "").replace("B", "").replace("+",""))*mul)

def prettify_row(row):
    skip = False
    result = {
      "repo": row["id"],
      "stars": row["star_count"],
      "pulls": format_pull_count(row["rate_plans"][0]["repositories"][0]["pull_count"]),
      "description": row["short_description"] if len(row["short_description"]) < 20 else row["short_description"][:37]+"...",
      "updated": to_unixtime(row["updated_at"])
    }
    if (TIME_NOW - result["updated"])/86400 > args.updated_over_last_days:
      skip = True
    elif result["pulls"] < args.min_pulls:
      skip = True
    elif result["stars"] < args.min_stars:
      skip = True
    
    return result if skip is False else False


first_iter = True
for start_result in range(0, args.max_results, PAGE_SIZE):
    try:
        with req.urlopen(f"{url_with_args}&from={start_result}&size={PAGE_SIZE}") as r:
            json_body = r.read()
            for repo in json.loads(json_body)["results"]:
                row = prettify_row(repo)
                if row:
                    if first_iter:
                        print('\t'.join("["+s.upper()+"]" for s in row.keys()))
                        first_iter = False
                    print('\t'.join(str(s) for s in row.values()))
    except Exception as e:
      if args.verbose: print(e)
      sys.exit(0)
