#!/usr/bin/env python3

import requests, sys, json, datetime


### LICENSE: GNU GENERAL PUBLIC LICENSE. Version 3 ###


def print_usage():
    print('''
Usage examples:
    dockertags.py image=busybox
or
    dockertags.py image=python sort=date reverse=true os=windows arch=amd64
or
    dockertags.py image=mongo os=windows
or
    dockertags.py image=debian arch=arm64 sort=size reverse=true
etc..

Argumenst:
    image=image_name
    sort=[name, size, date]
    reverse=[true,false]
    os=[linux, windows]
    arch=[amd64, arm, arm64, 386, mips64le, ppc64le, s390x] 
          ''')

def get_tags_list(arg_img_name, start_url, set_arch, set_os):
    try:
        dict_ans = (json.loads(requests.get(url=start_url).text))
    except json.decoder.JSONDecodeError:
        print('Image "'+arg_img_name+'" not found!')
        exit(1)
    if not dict_ans.get('count'): return {}
    cur_page = start_url
    tags_info = [dict_ans]

    while cur_page:
        try:
            dict_ans = (json.loads(requests.get(url=cur_page).text))
        except requests.exceptions.MissingSchema:
            print('Image "'+arg_img_name+'" not found!')
            exit(1)
        tags_info.append(dict_ans)
        cur_page = dict_ans.get('next')

    strings = {}
    for key in [lkey.get('results') for lkey in tags_info]:
        for subkey in key:
            match = False
            tag_name = str(subkey.get('name'))
            sys_reqs = [ [image_info.get('os'),image_info.get('architecture'),'{:.1f}'.format(image_info.get('size')/1024/1024), image_info.get('digest') ] for image_info in subkey.get('images') ]
            for sys_req in sys_reqs:
                if set_arch in sys_req and set_os in sys_req and sys_req[3]:
                    image_os, image_arch, image_size, digest  = sys_req
                    match = True
                    break
                elif (not (sys_req[1]) and (set_os == sys_req[0])) or ((not sys_req[0]) and (set_arch == sys_req[1])):
                    image_os, image_arch, image_size, digest  = [sys_req[0] or str(None),sys_req[1] or str(None),sys_req[2] or str(None), sys_req[3] or tag_name]
                    match = True
                elif not sys_req[0] and not sys_req[1]:
                    image_os, image_arch, image_size, digest  = sys_req
                    match = True
            if not match: continue

            if subkey.get('images') != []:
                last_pushed_date = subkey.get('tag_last_pushed')
                if last_pushed_date:
                    last_pushed_date = str(datetime.datetime.strptime(last_pushed_date.split('.')[0], "%Y-%m-%dT%H:%M:%S"))
            else:
                last_pushed_date = str(None)
            strings[digest+':'+tag_name]=[tag_name,image_os,image_arch,float(image_size),last_pushed_date]
    return strings


try:
    args_dict = {}
    for arg in sys.argv[1:]:
        if arg.count('=') > 1:
            print('Syntax error, multiple "="')
            exit(1)
        if arg.count('=') < 1:
            print('Syntax error, delimeter "=" not found')
            exit(1)
        if arg.index('=') in [0, len(arg)]:
            print('Syntax error, extra spaces or "="')
            exit(1)
        var, val = arg.split('=')
        args_dict[var] = val
    img_name = args_dict.get('image')
    if not img_name:
        print('Error, image name is not defined!')
        exit(1)
except:
    print_usage()
    exit(1)

try:
    sorting_columns = {'tag':0,'os':1,'arch':2,'size':3,'date':4}
    set_arch = args_dict.get('arch') or 'amd64'
    set_os = args_dict.get('os') or 'linux'
    sort_by = sorting_columns.get(args_dict.get('sort')) or sorting_columns.get('date')
    reverse = True if str(args_dict.get('reverse')).lower() == 'true' else False
except:
    print('Syntax error, check input arguments.')
    print_usage()
    exit(1)


start_url='https://hub.docker.com/v2/repositories/library/'+img_name+'/tags?page=1&page_size=1024'  

strings = get_tags_list(img_name, start_url, set_arch, set_os)
if strings:
    print(' '*(len(img_name)+1)+f"{'NAME' : <40}{'OS' : <10}{'ARCH' : <10}{'SIZE' : <10}{'UPDATE DATE' : <10}")
    for string in sorted(strings.values(), key = lambda values: values[sort_by], reverse=reverse):
        print(img_name+":",end='')
        print(f"{string[0] : <40}{string[1] : <10}{string[2] : <10}{str(string[3])+'MB' : <10}{string[4] : <10}")
else:
        print('Image "'+img_name+'" not found!')
